from fastapi import APIRouter, Path, Query, Depends
from typing import List
from middlewares.jwt_bearer import JWTBearer
from config.database import Session
from models.movie import Movie as MovieModel
from services.movie import MovieService
from schemas.movie import Movie
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder


movie_router = APIRouter()


@movie_router.get('/movies', tags=["movies"], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


@movie_router.get('/movies/{id}', tags=["movies"])
def get_movies_by_id(id: int = Path(default=1, ge=1, le=2000)):
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Esa pelicula no esta disponible!!"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


@movie_router.get('/movies/', tags=['movies'], response_model=List[Movie])
def get_movies_by_category(category: str = Query(default="Drama", min_length=2, max_length=15)) -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies_by_category(category)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Esa categoría no esta disponible!!"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


@movie_router.post('/movies', tags=["movies"], response_model=List[Movie], status_code=201)
def create_movie(movie: Movie) -> List[Movie]:
    db = Session()
    MovieService(db).create_movie(movie)
    return JSONResponse(status_code=201, content={"message": "Se ha registrado la película"})


@movie_router.put('/movies/{id}', tags=["movies"], response_model=dict)
def edit_movie(id: int, movie: Movie) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Esa pelicula no esta disponible!!"})
    MovieService(db).edit_movie(id, movie)
    return JSONResponse(status_code=200, content={"message": "La película se ha editado correctamente!!"})


@movie_router.delete('/movies/{id}', tags=['movies'], response_model=dict, status_code=200)
def delete_movie(id: int) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Esa pelicula no esta disponible!!"})
    MovieService(db).delete_movies(id)
    return JSONResponse(status_code=200, content={"message": "La película se ha eliminado correctamente!!"})
