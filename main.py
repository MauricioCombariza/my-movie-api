from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.users import user_router


app = FastAPI()

app.title = "FastAPI, jwt y base de datos"
app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)

Base.metadata.create_all(bind=engine)


movies = [
    {
        'id': 1,
        'title': 'Avatar',
        'overview': "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
        'year': '2009',
        'rating': 7.8,
        'category': 'Acción'
    },
    {
        'id': 2,
        'title': 'Titanic',
        'overview': "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
        'year': '1995',
        'rating': 7.8,
        'category': 'Drama'
    },
    {
        'id': 3,
        'title': 'Duro de matar 34',
        'overview': "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
        'year': '2009',
        'rating': 7.8,
        'category': 'Acción'
    }
]

# El title sirve para darle un título a mi documentación
app.title = "Mi aplicación con fastAPI"
# app.version = "0.0.1"

# Los tags son para agrupar mis endpoint


@app.get('/', tags=["home"])
def message():
    return HTMLResponse('<h1 style=color:blue> Hola Mauricio </h1>')
